//  CRUD Operations

/* 
	C - Create (Insert document/s)
	R - Read/Retrieve (View specific/all document/s)
	U - Update (Edit specific document/s)
	D - Delete (Remove specific document/s)

	-   CRUD Operations are the heart of any backend application.
*/



//  [SECTION]   Insert a documents (CREATE)

/* 
		Syntax:

			db.collectionName.insertOne({object});


		Comparison with javascript
	    
			object.object.method({object})

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123455",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Phython"],
	department: "none"
});



// Insert a single document
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with basic necessities",
	rooms_available: 10,
	isAvailable: false
});


// Insert Many

/* 
	Syntax:

		db.collectionName.InsertName([{objectA}, {objectB}])

*/

db.users.insertMany([{
	firstName: "test",
	lastName: "",
	age: 25,
	contact: {
		phone: "123455",
		email: "test@gmail.com"
	},
	courses: ["React", "JavaScript", "Phython"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "123455",
		email: "neiarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}
]);


db.rooms.insertMany([{
	name: "double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
},
{
	name: "queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
}
]);





//  [SECTION]   Retrieve a document (READ)

/* 

Syntax:

	db.collectionName.find({}); // get all the documents
	db.collectionName.find({field:value}); // find a specific document.
	
*/
// get 
db.users.find({});

// find all the documents in the collection
db.users.find({ firstName: "test" });

db.users.find({ department: "none" });


// Find documents with multiple parameters.

/* 
	db.collectionName.find({fieldA : valueA, fieldB : valueB)

*/

db.users.find({ lastName: "", age: 25 });




// [SECTION]    Updating Documents (UPDATE)

// Create a document to update
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "TestEmail@gmail.com"
	},
	course: [],
	department: "none"
});

/* 
- Just like the find method, methods that only manipulate a single document will only apply

Syntax:

	db.collectionName.updateOne({criteria}, {$set:
		{field: value}});

*/

db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			course: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"

		}

	}
);


// Updating multiple documents

/* 
Syntax:

	db.collectionName.updateMany({criteria}, {$set: {field: value}});

*/


db.users.updateMany(
	{ department: "none" },
	{
		$set:
			{ department: "HR" }
	});



db.users.updateOne(
	{ _id: ObjectId("6307370f0e6ab0cd554e3329") },
	{
		$set:
			{ phone: 5555 }
	});


// Replace One

/* 
	- Can be used if replacing the whole document if necessary.
Syntax: 

	db.collectionName.replaceOne({criteria}, {field: value});
*/


db.users.replaceOne({
	firstName: "Bill"
}, {
	firstName: "Bill",
	lastName: "Gates",
	age: 65,
	contact: {
		phone: "12345678",
		email: "bill@gmail.com"
	},
	course: ["PHP", "Laravel", "HTML"],
	department: "Operations"
}
);



//  [SECTION] Removing documents (DELETE)

/* 
Deleting a single document

	Syntax:

		db.collectionName.deleteOne({criteria}); // deletes the first document

*/

db.users.deleteOne({ firstName: "Test" });


/* 
Delete Many 
*/
db.users.deleteMany({ firstName: "Test" });




// [SECTION] Advanced queries

/* 
	
*/

//  Query an embedded document

db.users.find({
	contact: {
		phone: "123455",
		email: "test@test.com"
	}
});


//  Query on nested field/subdocument
db.users.find({
	"contact.email": "test@test.com"
});


//  Querying an array with Exact Elemet
db.users.find({
	course: ["PHP", "Laravel", "HTML"],
});

//  Querying an array disregarding the array elements order.
db.users.find(
	{
		course: { $all: ["PHP", "HTML", "Laravel"] }
	}
);


// Querying an Embedded Array

db.users.insertOne({
	name: [
		{
			name: "John"
		},
		{
			nameB: "Glenn"
		}
	]
});

db.users.find({
	name: {
		nameA: "John"
	}
})